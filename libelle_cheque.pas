Program libelle_cheque;

//Définition du typle tableau à 4 cases
Type tableau = array [1..4] Of integer;
//tableau contenant les différents paquets d'entier après découpage

Var 
  valeur : real; //Valeur rentrée par l'utilisateur
  nb_caracteres, compt : integer; //Nombre de caractères avant la virgule | un compteur
  resultat : string; //Stockage du résultat après transcription
  tab : tableau; //Tableau, stockage après découpage

//Fonction pour traiter les centaines et les unités > 6
//Retourne la chaine de caractères correspondant au chiffre en paramètre
Function traitement_speUnit(unispe:integer) : string;
begin
  case unispe of
    0 : traitement_speUnit := ' dix' ;
    1 : traitement_speUnit := ' onze' ;
    2 : traitement_speUnit := ' douze' ;
    3 : traitement_speUnit := ' treize' ;
    4 : traitement_speUnit := ' quatorze' ;
    5 : traitement_speUnit := ' quize' ;
    6 : traitement_speUnit := ' seize' ;
    7 : traitement_speUnit := ' dix sept';
    8 : traitement_speUnit := ' dix huit';
    9 : traitement_speUnit := ' dix neuf';
  end;
end;

//Fonction pour traiter les centaines et unité normales
//Prend en paramètre le chiffre à traduire
//Retourne une chaine de caractère
function traitement_chiffre(uni:integer) : string;
begin
  case uni of
    0 : traitement_chiffre := '';
    1 : traitement_chiffre := '';
    2 : traitement_chiffre := ' deux';
    3 : traitement_chiffre := ' trois';
    4 : traitement_chiffre := ' quatre';
    5 : traitement_chiffre := ' cinq';
    6 : traitement_chiffre := ' six';
    7 : traitement_chiffre := ' sept';
    8 : traitement_chiffre := ' huit';
    9 : traitement_chiffre := ' neuf';
  end;
end;

//Fonction pour traiter les dizaines
//Prend en paramètre le chiffre à traiter
//Retourne la chaine de caractères correspondant au chiffre en paramètre
function traitement_dix(dix:integer) : string;
begin
  case dix of
    0 : traitement_dix := '';
    1 : traitement_dix := '';
    2 : traitement_dix := ' vingt';
    3 : traitement_dix := ' trente';
    4 : traitement_dix := ' quarante';
    5 : traitement_dix := ' cinquante';
    6 : traitement_dix := ' soixante';
    7 : traitement_dix := ' soizante';
    8 : traitement_dix := ' quatre-vingt';
    9 : traitement_dix := ' quatre-vingt';
  end;
end;

function separation(sepa : integer) : string;
begin
    case sepa of
        1 : separation := ' million';
        2 : separation := ' mille' ;
    end;
    if (sepa = 3) and (tab[1] <> 0) and (tab[2] = 0) and (tab[3] = 0) then
    begin
      separation := ' d''euro';
    end
    else
      if sepa = 3 then
      Begin
        separation := ' euro';
      end;
end;

//Fonction de traitement de chaque groupe de 3
//Prend en paramètre le nombre de 3 chiffres à traiter
//Retourne la chaine de caractères correspondant au paramètre
function traitement(nb:integer) : string;
Var 
  centaine, dizaine, unite : integer;
  chainetmp : string;
begin
  //Définition des différente partie du nombre
  centaine := trunc(nb/100);
  dizaine := trunc(nb/10) - centaine*10;
  unite := nb - dizaine*10 - centaine*100;
  
  //Initialise la chaine de retour à vide
  chainetmp := '';

  //Création de la chaine de caractères, partie centaine si non nul
  if centaine <> 0 then
  chainetmp := chainetmp + traitement_chiffre(centaine) + ' cent';

  //Ajout d'un 's' à cent si la valeur totale est : 200, 300, 400, 500, 600, 700, 800 ou 900; pile
  if (trunc(valeur) mod 100 = 0) and (valeur <> 100) and (valeur < 1000) and (nb = 100) then
    chainetmp := chainetmp + 's';

  //Création de la chaine de caractères, partie dizaine
  if dizaine <> 0 then
  begin
    chainetmp := chainetmp + traitement_dix(dizaine);
    //Ajout d'un 's' si la valeur totale est 80 pile
    if (trunc(valeur) = 80) and (nb = 80) then 
      chainetmp := chainetmp + 's';
  end;

  //Création de la chaine de caractères, partie unité
  //Ajout du 'et' de exemple 'vingt et un' sauf pour dizaine 1, 8 et 9
  if (unite = 1) and (dizaine <> 9) and (dizaine <> 8) and (dizaine <> 1) and (dizaine <> 0) then
    chainetmp := chainetmp + ' et';
  //Ajout du 'un' si dizaine différente de 1, 7 et 9
  if (unite = 1) and (dizaine <> 1) and (dizaine <> 7) and (dizaine <> 9) then
    chainetmp := chainetmp + ' un';
  //Traitement spécial pour les unité < 7 si dizaine 1, 7 ou 9, car pas 'dix-un' mais 'onze'
  if (dizaine = 1) or (dizaine = 7) or (dizaine = 9) then 
    chainetmp := chainetmp + traitement_speUnit(unite)
  else
  //Sinon traitement normal, cf traitement_chiffre
  begin
    chainetmp := chainetmp + traitement_chiffre(unite);
  end;
  //Retour de la fonction
  traitement := chainetmp;
end;

//Procédure demandant la saisie avec controle de saisie
Procedure saisie();
Var 
  verifToReal : integer;
  chaine : String;
Begin
  //Vérification de Saisie et si positif et dans l'interval
  Repeat
    writeln('Veillez saisir un nombre compris entre 0,01 et 999 999 999,99.');
    writeln('Celui-ci sera transcrit en lettres.');
    readln(chaine);
    val(chaine, valeur, verifToReal);
    //Essaye de convertir la chaine:string en valeur:real
    //verifReal prend 0 si réussite, autre sinon
  Until ((verifToReal = 0) And (valeur >= 0.01) And (valeur <= 999999999.99) and (trunc(valeur*100)/100 = valeur));
  {
    On boucle jusqu'à ce que :
    - La valeur saisie dans un string peut être convertie en real
    - La valeur saisie est compris dans l'intervalle 0.01 et 999 999 999.99
    - La valeur n'a pas plus de 2 chiffres après la virgule
  }
End;

//Procédure qui compte le nombre de chiffre avant la virgule
//Stocke la valeur dans nb_caracteres
Procedure compteur();
Var
  chaine : string;
Begin
  Str(trunc(valeur), chaine);
  //Récupère la taille de la chaine de caractère tronquée à l'unité
  nb_caracteres := Length(chaine);
End;


//Découpe en 4 parties, 3 au avant la virgule : millons, millers et unités
//Et un après la virgule
Procedure decoupe();
begin
  //La fonction trunc ne garde que la partie entière du réel
  tab[1] := trunc(valeur/1000000);
  tab[2] := trunc(valeur/1000 - tab[1]*1000);
  tab[3] := trunc(valeur) - tab[2]*1000 - tab[1]*1000000;
  tab[4] := trunc(valeur*100) - trunc(valeur)*100;
end;


Procedure transcription();
begin
  //Initialisation du résultat
  resultat := '';
  //Traitement avant la virgule des 3 groupes
  for compt := 1 to 3 do
  begin
    //A ne lancé que si le nombre est non nul
    if (tab[compt] <> 0) or (compt = 3) then
    begin
    //Si compt différent de 2 ou, si compt = 2 le nombre est différent de 1
    //Car pas un mille mais mille
      if ((compt = 2) and (tab[compt] <> 1)) or (compt <> 2) then
        resultat := resultat + traitement(tab[compt]);
      resultat := resultat + separation(compt);
    //Ajout du 's' à millions si plus ou 2 millions
      if (compt = 1) and (tab[1] >= 2) then
        resultat := resultat + 's';
    end;
  end;

//Si la valeur avant la virgule est 0
  if trunc(valeur) = 0 then
  begin
    resultat := ' zero';
    //Ajour du 'euro'
    resultat := resultat + separation(3);
  end;

  //Ajout du 's' à euro dans la valeur est supérieur ou égale à 2
  if valeur >= 2 then
    resultat := resultat + 's';
  
  //Traitement après la virgule, si non nul
  if tab[4] <> 0 then
  begin
    resultat := resultat + ' et' + traitement(tab[4]) + ' centime';
    //Ajout du 's' si plus de 1 centime
    if tab[4] > 1 then
      resultat := resultat + 's';
  end;
end;

//Exécution du programme
Begin
  saisie();
  decoupe();
  transcription();
  writeln(resultat);
End.